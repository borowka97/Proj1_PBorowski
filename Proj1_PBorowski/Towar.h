#pragma once
#include <string>
#include <iostream>


class Towar
{
protected:
	std::string name;
	std::string id;
	float price;
	int amount;


public:
	Towar(std::string, float, int);
	Towar();
	virtual ~Towar();


	
	//Setters
	void Set_name(std::string);
	void Set_price(float);
	void Set_amount(int);

	//Getters
	std::string Get_name() const;
	std::string Get_ID() const;
	float Get_price() const;
	int Get_amount() const;

	//printing
	virtual void Print(std::ostream&)const = 0;
	friend std::ostream & operator<< (std::ostream&, const Towar&);
	virtual void Print(std::ofstream &)const = 0;

};

