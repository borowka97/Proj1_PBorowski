#include "stdafx.h"
#include "Towar_RTV.h"
#include <string>
#include <fstream>


int Towar_RTV::number;
std::string Towar_RTV::PREFIX = "RTV";

Towar_RTV::Towar_RTV()
{
	id = PREFIX + std::to_string(number);
	number++;
}

Towar_RTV::Towar_RTV(std::string n, float pr, int am,int pow):Towar(n,pr,am)
{
	id = PREFIX + std::to_string(number);
	power = pow;
	number++;
}


Towar_RTV::~Towar_RTV()
{
}

void Towar_RTV::Print(std::ostream &out) const
{
	out.width(15);
	out << std::left << Get_name() << "\t" << Get_price() << "\t" << Get_amount()  << "\t" << Get_power() <<" " << "\t\t" << Get_ID();
}

void Towar_RTV::Print(std::ofstream & out) const
{
	out << Get_ID() << " " << Get_name() << " " << Get_price() << " " << Get_amount() << " " << Get_power() <<std::endl;
}

int Towar_RTV::Get_power() const
{
	return Towar_RTV::power;
}

int Towar_RTV::Get_number()
{
	return number;
}

std::string Towar_RTV::Get_PREFIX()
{
	return PREFIX;
}

void Towar_RTV::Set_power(int pow)
{
	power = pow;
}

void Towar_RTV::Set_number(int numb)
{
	number = numb;
}

std::ostream & operator<<(std::ostream &out, const Towar_RTV &item)
{
	item.Print(out);
	return out;
}
