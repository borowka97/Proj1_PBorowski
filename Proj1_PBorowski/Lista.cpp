#include "stdafx.h"
#include "Lista.h"
#include "Towar.h"
#include <iostream>
#include <vector>

using namespace std;

vector<Lista*> Lista::lists;

Lista::Lista()
{
	name = "noname";
	lists.push_back(this);
}

Lista::Lista(std::string name)
{
	Lista::name = name;
	lists.push_back(this);
}


Lista::~Lista()
{
	for (size_t i = 0; i < list.size(); i++)
		delete list[i];
	
	cout << "Usuwam: " << this->Get_Name();
	system("pause");
}

size_t Lista::Length()
{
	return list.size();
}

std::string Lista::Get_Name()
{
	return name;
}

bool Lista::Add(Towar * towar)
{
	list.push_back(towar);
	return true;
}

bool Lista::Delete(Towar * item)
{
	for (size_t i = 0; i < list.size(); i++)
	{
		if (list[i] == item)
		{
			list.erase(list.begin() + i);
			return true;
		}
	}
	return false;
}

void Lista::Delete(size_t position)
{
	list.erase(list.begin() + position);
}

void Lista::Edit(Towar * item, std::string name, float price, int amount)
{
	item->Set_name(name);
	item->Set_price(price);
	item->Set_amount(amount);

}

void Lista::Edit(size_t position, std::string name, float price, int amount)
{
	list.at(position)->Set_name(name);
	list.at(position)->Set_price(price);
	list.at(position)->Set_amount(amount);
}

Towar* Lista::Find(std::string x)
{
	for (size_t i = 0; i < list.size(); i++)
	{
		if (list[i]->Get_name() == x)
			return list[i];
	}
	return nullptr;
}

void Lista::Print()
{
	for (size_t i = 0; i < list.size(); i++)
	{
		std::cout << i+1 <<". " << *list[i] << "\n";
	}
}

bool Lista::Print(std::string name)
{
	std::string file_extension = ".txt";
	name += file_extension;

	ofstream file(name);

	if (file.good() == true)
	{
		for (size_t i = 0; i < list.size(); i++)
		{
			list[i]->Print(file);
		}
		file.close();
		return true;
	}

	return false;
}

Towar& Lista::operator[](size_t x)
{
	return *list[x];
}

