#pragma once
#include "Towar.h"
#include "Towar_RTV.h"
#include "Towar_spozywczy.h"
#include "Towar_AGD.h"
#include <string>
#include <vector>
#include <fstream>


class Lista
{
protected:
	std::string name;
	std::vector <Towar*> list;

public:
	static std::vector<Lista*> lists;
	
	Lista();
	Lista(std::string);
	~Lista();

	size_t Length();
	std::string Get_Name();

	bool Add(Towar*);
	bool Delete(Towar*);
	void Delete(size_t);
	void Edit(Towar*, std::string name, float price, int amount);
	void Edit(size_t, std::string, float, int);
	void Print();
	bool Print(std::string);

	Towar* Find(std::string);
	
	Towar & operator[](size_t);

	};