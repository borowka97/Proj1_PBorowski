#include "stdafx.h"
#include "Towar_AGD.h"
#include <fstream>

int Towar_AGD::number;
std::string Towar_AGD::PREFIX = "AGD";

Towar_AGD::Towar_AGD(std::string n, float pr, int am, char energy): Towar(n,pr,am)
{
	energy_class = energy;
	id = PREFIX + std::to_string(number);
	number++;
}

Towar_AGD::Towar_AGD()
{
	id = PREFIX + std::to_string(number);
	number++;
}

Towar_AGD::~Towar_AGD()
{
}

char Towar_AGD::Get_energy_class() const
{
	return energy_class;
}

int Towar_AGD::Get_number()
{
	return number;
}

std::string Towar_AGD::Get_PREFIX() 
{
	return PREFIX;
}

void Towar_AGD::Set_energy_class(char energy)
{
	energy_class = energy;
}

void Towar_AGD::Set_number(int numb)
{
	number = numb;
}

void Towar_AGD::Print(std::ostream &out) const
{
	out.width(15);
	out << std::left << Get_name() << "\t" << Get_price() << "\t" << Get_amount() << " \t" << Get_energy_class() << "\t\t" << Get_ID();
}

void Towar_AGD::Print(std::ofstream & out) const
{
	out << Get_ID() << " " << Get_name() << " " << Get_price() << " " << Get_amount() << " " << Get_energy_class() <<std::endl;
}

std::ostream & operator<<(std::ostream &out, const Towar_AGD &item)
{
	item.Print(out);
	return out;
}
