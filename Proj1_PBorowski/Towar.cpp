#include "stdafx.h"
#include "Towar.h"


Towar::Towar(std::string name, float price, int amount):name(name),price(price),amount(amount)
{
}

Towar::Towar()
{

}

Towar::~Towar()
{
}

void Towar::Print(std::ostream & out) const
{
//	out << Get_name() << " " << Get_price() << "zl " << Get_amount() << " " << Get_ID();
}

void Towar::Set_name(std::string x)
{
	Towar::name = x;
}

void Towar::Set_price(float x)
{
	Towar::price = x;
}

void Towar::Set_amount(int x)
{
	Towar::amount = x;
}

std::string Towar::Get_name() const
{
	return Towar::name;
}

std::string Towar::Get_ID() const
{
	return Towar::id;
}

float Towar::Get_price() const
{
	return Towar::price;
}

int Towar::Get_amount() const
{
	return Towar::amount;
}

std::ostream & operator<<(std::ostream &out, const Towar &towar)
{
	towar.Print(out);
	return out;
}
