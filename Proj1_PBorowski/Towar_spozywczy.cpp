#include "stdafx.h"
#include "Towar_spozywczy.h"
#include <fstream>

int Towar_spozywczy::number;
std::string Towar_spozywczy::PREFIX = "SPZ";

Towar_spozywczy::Towar_spozywczy(std::string name, float price, int amount, std::string exp_date, float weight): Towar(name,price,amount), expiration_date(exp_date), weight(weight)
{
	id = PREFIX + std::to_string(number);
	number++;
}

Towar_spozywczy::Towar_spozywczy()
{
	id = PREFIX + std::to_string(number);
	number++;
}

Towar_spozywczy::~Towar_spozywczy()
{
}

std::string Towar_spozywczy::Get_expiration_date() const
{
	return expiration_date;
}

float Towar_spozywczy::Get_weight() const
{
	return weight;
}

int Towar_spozywczy::Get_number()
{
	return number;
}

std::string Towar_spozywczy::Get_PREFIX()
{
	return PREFIX;
}

void Towar_spozywczy::Set_weight(float w)
{
	weight = w;
}

void Towar_spozywczy::Set_expiration_date(std::string exp)
{
	expiration_date = exp;
}

void Towar_spozywczy::Set_number(int numb)
{
	number = numb;
}

void Towar_spozywczy::Print(std::ostream &out) const
{
	out.width(15);
	out << std::left << Get_name() << "\t" << Get_price() << "\t" << Get_amount() << "\t"<< Get_expiration_date() << " " << Get_weight() <<"\t" << Get_ID();
}

void Towar_spozywczy::Print(std::ofstream &out) const
{
	out << Get_ID() << " " << Get_name() << " " << Get_price() << " " << Get_amount() << " " << Get_expiration_date() << " " << Get_weight()<<std::endl;
}

std::ostream & operator<<(std::ostream &out, const Towar_spozywczy &item)
{
	item.Print(out);
	return out;
}