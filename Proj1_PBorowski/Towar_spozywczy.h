#pragma once
#include "Towar.h"
class Towar_spozywczy :
	public Towar
{
private:
	std::string expiration_date;
	float weight;

	static std::string PREFIX;
	static int number;

public:
	Towar_spozywczy(std::string, float, int, std::string , float);
	Towar_spozywczy();
	~Towar_spozywczy();

	//getters
	std::string Get_expiration_date() const;
	float Get_weight() const;
	static int Get_number();
	static std::string Get_PREFIX();

	//setters
	void Set_weight(float);
	void Set_expiration_date(std::string);
	static void Set_number(int);

	//printing
	void Print(std::ostream&) const;
	friend std::ostream & operator<< (std::ostream &, const Towar_spozywczy&);
	void Print(std::ofstream &)const;
};

