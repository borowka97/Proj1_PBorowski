#pragma once
#include "Towar.h"

class Towar_AGD :
	public Towar
{
private:
	char energy_class;
	static int number;
	static std::string PREFIX;

public:
	Towar_AGD(std::string, float, int, char);
	Towar_AGD();
	~Towar_AGD();

	//getters
	char Get_energy_class() const;
	static int Get_number();
	static std::string Get_PREFIX();

	//setters
	void Set_energy_class(char);
	static void Set_number(int);

	//printing
	void Print(std::ostream&) const;
	friend std::ostream & operator<< (std::ostream &, const Towar_AGD&);
	void Print(std::ofstream &)const;

};

