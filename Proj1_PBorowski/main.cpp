#include "stdafx.h"
#include "Towar.h"
#include "Lista.h"
#include <iostream>

using namespace std;




Lista* IdControl(string);
void IdSave(string);

//funckje ogolne list
Lista* AddNewList();
void DeleteList(vector<Lista*>*);
Lista* LoadNewList(string);
void EditList(Lista*);

Lista * ChangeList(vector<Lista*>*);

//funckje na towarach w listach
void AddProduct(Lista*);

int main()
{
	int choose;
	string name;
	Lista* list = nullptr;//new Lista("nowa");
	string ctrl_file = "id_ctrl.txt";

	list = IdControl(ctrl_file);
	

	/*
	Towar * tv = new Towar_RTV("okil", 656.5f, 4, 1900);
	Towar * mieso = new Towar_spozywczy("karkowka", 30.6f, 10, "21.11.2019", 1.8f);
	Towar * radio = new Towar_RTV("radio", 99.99f, 50, 90);
	Towar * pralka = new Towar_AGD("pralka", 1199.99f, 1,'A');
	list->Add(tv);
	list->Add(mieso);
	list->Add(radio);
	list->Add(pralka);
	*/
	//TODO: ZROBIC STATYCZNA TABLICE W LISTA KTORA PRZECHOWUJE WSZYSTKIE LISTY
	while (true)
	{
		system("cls");
		if (list != nullptr) cout << "Aktywna lista: " <<  list->Get_Name() << endl << endl;
		cout << "MENU:" << endl;
		if (list != nullptr) cout << "1. Wyswietl liste." << endl;
		cout << "2. Dodaj liste" << endl;
		if (list != nullptr) cout << "3. Usun liste" << endl;
		if (list != nullptr) cout << "4. Zapisz liste" << endl;
		cout << "5. Importuj liste" << endl;
		if (list != nullptr) cout << "6. Zmien liste" << endl;
		if (list != nullptr) cout << "7. Edytuj liste" << endl;
		cout << "0. Zapisz i zamknij" << endl;

		cin >> choose;

		switch (choose)
		{
			case 1:
				if (list != nullptr)
				{
					list->Print();
					system("pause");
				}
				break;
			case 2:
				list = AddNewList();
				break;
			case 3:
				if (list != nullptr)
				{
					list = nullptr;
					DeleteList(&Lista::lists);
					if (Lista::lists.empty() == 0) list = Lista::lists[0];
				}
				break;
			case 4:
				if (list != nullptr)
				if(list->Print(list->Get_Name())) cout << "Zapisano.";
				break;
			case 5:
				cout << "Podaj nazwe pliku (bez rozszerzenia) nowej listy: ";
				cin >> name;
				list = LoadNewList(name);
				break;
			case 6:
				if (list != nullptr)
				list = ChangeList(&Lista::lists);
				break;
			case 7:
				if (list != nullptr)
				EditList(list);
				break;
			case 0:
				IdSave(ctrl_file);
				exit(0);
			default:
				break;

		}
	}

    return 0;
}


Lista* IdControl(string ctrl_file)
{
	ifstream file(ctrl_file, ios_base::in);
	Lista* list = nullptr;

	string id;
	string name;
	int id_rtv, id_agd, id_spz;

	if (file.good() == 1)
	{
		while (!file.eof())
		{
			file >> id;

			if (id.substr(0, 3) == Towar_RTV::Get_PREFIX()) id_rtv = stoi(id.substr(3));
			if (id.substr(0, 3) == Towar_AGD::Get_PREFIX()) id_agd = stoi(id.substr(3));
			if (id.substr(0, 3) == Towar_spozywczy::Get_PREFIX()) id_spz = stoi(id.substr(3));
			if (id.substr(0, 3) == "$^$")
			{
				cout << "WCZYTYWANIE LISTY ";
				while (!file.eof())
				{
					file >> name;
					if (file.eof()) break;
					cout << name << endl;
					list = LoadNewList(name);
					//system("pause");
				}
			}


		}
		Towar_RTV::Set_number(id_rtv);
		Towar_AGD::Set_number(id_agd);
		Towar_spozywczy::Set_number(id_spz);
	}

	return list;
}

void IdSave(string ctrl_file)
{
	ofstream file(ctrl_file, ios_base::out);

	file << "RTV" << Towar_RTV::Get_number() << " " << "AGD" << Towar_AGD::Get_number() << " " << "SPZ" << Towar_spozywczy::Get_number() << endl << "$^$" << endl;
	for (size_t i = 0; i < Lista::lists.size(); i++)
	{
		file << Lista::lists.at(i)->Get_Name() << endl;
		Lista::lists.at(i)->Print(Lista::lists.at(i)->Get_Name());
	}
	file.close();
}

Lista* AddNewList()
{
	std::string name;
	cout << "Podaj nazwe nowej listy: ";
	cin >> name;
	return new Lista(name);
}

Lista* LoadNewList(string file_name)
{
	string type;

	string name;
	float price;
	int amount;

	string file_extension = ".txt";
	Lista* new_list = new Lista(file_name);
	file_name += file_extension;
	ifstream file(file_name);

	if (file.good() == 1)
	{
		while (!file.eof())
		{
			file >> type;
			if (file.eof()) break;
			file >> name;
			file >> price;
			file >> amount;

			if (type.substr(0, 3) == Towar_RTV::Get_PREFIX()) 
			{
				int power;
				file >> power;

				new_list->Add(new Towar_RTV(name,price,amount,power));
				cout << "Wczytalem rtv" << endl;
			}
			if (type.substr(0, 3) == Towar_AGD::Get_PREFIX()) 
			{
				char energy_class;
				file >> energy_class;

				new_list->Add(new Towar_AGD(name, price, amount,energy_class ));
				cout << "Wczytalem agd" << endl;
			}
			if (type.substr(0, 3) == Towar_spozywczy::Get_PREFIX()) 
			{
				float weight;
				string expiration_date;

				file >> expiration_date;
				file >> weight;

				new_list->Add(new Towar_spozywczy(name, price, amount, expiration_date, weight));
				cout << "Wczytalem spz" << endl;
			}
		}
		file.close();
		return new_list;
	}

	return nullptr;
}

void DeleteList(vector<Lista*>* lists)
{
	size_t choose;

	cout << "Usun liste: " << endl;
	for (size_t i = 0; i < lists->size(); i++)
	{
		cout << i + 1 << " " << lists->at(i)->Get_Name() << std::endl;
	}

	do
	{
		cin >> choose;
	} while (choose > lists->size() || choose < 1);

	delete (*lists)[choose-1];
	lists->erase(lists->begin() + choose - 1);
}

Lista * ChangeList(vector<Lista*>* lists)
{
	size_t choose;

	cout << "Zmien liste na: " << endl;
	for (size_t i = 0; i < lists->size(); i++)
	{
		cout << i + 1 << " " << lists->at(i)->Get_Name() << std::endl;
	}

	do
	{
		cin >> choose;
	} while (choose > lists->size() || choose < 1);

	return (*lists)[choose-1];
}

void EditList(Lista* list)
{
	system("cls");
	size_t choose;
	string name;
	float price;
	int amount;

	cout << "MENU EDYCJI LISTY "<< list->Get_Name() << endl << endl;
	cout << "1. Wyswietl liste" << endl;
	cout << "2. Dodaj produkt" << endl;
	if (list->Length() != 0)
	{
		cout << "3. Usun produkt" << endl;
		cout << "4. Edytuj produkt" << endl;
		cout << "5. Znajdz produkt" << endl;
	}
	cout << "0. Wroc" << endl;
	cin >> choose;

	switch (choose)
	{
	case 1:
		list->Print();
		system("pause");
		break;
	case 2:
		cout << "Podaj ile produktow chcesz dodac: ";
		cin >> choose;
		for (size_t i = 0; i < choose; i++)
		{
			AddProduct(list);
			cout << endl;
		}
		break;
	case 3:
		list->Print();
		cout << "Ktory produkt usunac?: ";
		cin >> choose;
		list->Delete(choose-1);
		break;
	case 4: 
		list->Print();
		cout << "Ktory produkt edytowac?: ";
		do { cin >> choose; } while (choose < 1 || choose > list->Length());
		cout << (*list)[choose-1] << endl << endl;
		cout << "Podaj nowa nazwe produktu: ";
		cin >> name;
		cout << "Podaj nowa cene jednostkowa produktu: ";
		cin >> price;
		cout << "Podaj nowa ilosc produktu: ";
		cin >> amount;
		list->Edit(choose-1, name, price, amount);
		break;
	case 5: 
		cout << "Podaj nazwe produktu do wyszukania: ";
		cin >> name;
		if (list->Find(name) == nullptr) cout << endl << "Brak produktu";
		else cout << *(list->Find(name)) << endl;
		system("pause");
		break;
	case 0:
		break;
	default:
		break;

	}
}

void AddProduct(Lista* list)
{
	size_t choose;
	string name;
	float price;
	int amount;

	cout << "Wybierz rodzaj produktu: " << endl;
	cout << "1. Produkt RTV" << endl;
	cout << "2. Produkt AGD" << endl;
	cout << "3. Produkt spozywczy" << endl;
	cout << "0. Przerwij" << endl;
	cin >> choose;
	if (choose == 0) return;
	cout << "Podaj nazwe produktu: ";
	cin >> name;
	cout << "Podaj cene jednostkowa produktu (w PLN): ";
	cin >> price;
	cout << "Podaj ilosc danych produktow: ";
	cin >> amount;

	if (choose == 1)
	{
		int power;
		cout << "Podaj moc produktu (w Watach): ";
		cin >> power;
		Towar * item = new Towar_RTV(name, price, amount, power);
		list->Add(item);
	}
	else if (choose == 2)
	{
		char energy_class;
		cout << "Podaj klase energetyczna (A,B,C,D): ";
		cin >> energy_class;
		Towar *item = new Towar_AGD(name, price, amount, energy_class);
		list->Add(item);
	}
	else if (choose == 3)
	{
		float weight;
		string expiration_date;
		cout << "Podaj wage jednego produktu: ";
		cin >> weight;
		cout << "Podaj date przydatnosci do spo�ycia (DD.MM.RR): ";
		cin >> expiration_date;
		Towar *item = new Towar_spozywczy(name, price, amount, expiration_date, weight);
		list->Add(item);
	}
	
	
}