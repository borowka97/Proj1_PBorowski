#pragma once
#include "Towar.h"
class Towar_RTV :
	public Towar
{
private:
	int power;
	
	static std::string PREFIX;
	static int number;

public:
	Towar_RTV(std::string,float,int,int);
	Towar_RTV();
	~Towar_RTV();

	//getters
	int Get_power() const;
	static int Get_number();
	static std::string Get_PREFIX();

	//setters
	void Set_power(int);
	static void Set_number(int);

	//printing
	void Print(std::ostream&) const;
	friend std::ostream & operator<< (std::ostream &, const Towar_RTV&);
	void Print(std::ofstream &)const;
};

